#! -*- coding:utf-8 -*-

import torch
torch.backends.cudnn.deterministic	= True
torch.backends.cudnn.benchmark		= False

# -------------------------------------------------------------------
import torch
def calculate_naively(model,data):
	grad1s = []
	for x,y in zip(data.x, data.y):
		x = x.unsqueeze(0)
		y = y.unsqueeze(0)

		logit = model(x)
		loss  = torch.nn.functional.cross_entropy(logit, y, reduction='sum')
		model.zero_grad()
		loss.backward()

		grad1 = [p.grad.flatten() for p in model.parameters()]
		grad1 = torch.cat(grad1)
		grad1s.append(grad1)
	return grad1s

import exgrads
def calculate_by_lib(model,data):
	return exgrads.grad1.vectorize(model, data.x, data.y)

# -------------------------------------------------------------------
import check_cases
import unittest
class TestMain(unittest.TestCase):
	def setUp(self):		pass
	def tearDown(self):		pass

	def test(self):
		print('='*50)
		error_tolerance = 1e-5

		for name,model,data in check_cases.patterns:
			model.eval()
			naive		= calculate_naively(model, data)
			efficient	= calculate_by_lib(model,  data)
			for i, (n,e) in enumerate(zip(naive, efficient)):
				error = ((n-e)**2).mean().sqrt().item()
				print(f'grad1_vectorize   name:{name:>8s} i:{i:02d} error:{error:8.6e}')
				self.assertTrue(error < error_tolerance)
			print('-'*50)

# -------------------------------------
if __name__ == '__main__':
	unittest.main()

# -------------------------------------