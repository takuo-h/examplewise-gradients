#! -*- coding:utf-8 -*-

import torch
torch.backends.cudnn.deterministic	= True
torch.backends.cudnn.benchmark		= False

import torch
import exgrads
import unittest
class TestMain(unittest.TestCase):
	def setUp(self):		pass
	def tearDown(self):		pass

	def test(self):
		print('='*50)
		
		batch,dim,label = 5,3,2
		x = torch.randn(batch,dim)                                  #: inputs
		y = torch.randint(low=0,high=label-1,size=(batch,))         #: outputs
		model   = torch.nn.Sequential(torch.nn.Linear(dim, label))  #: PyTorch model
		loss_fn = torch.nn.functional.cross_entropy                 #: loss function

		exgrads.hooks.register(model)
		model.zero_grad()
		loss_fn(model(x), y).backward()
		exgrads.hooks.compute_grad1(model)

		# param.grad:     gradient averaged over the batch
		# param.grad1[i]: gradient of i-th example
		for param in model.parameters():
			self.assertTrue(torch.allclose(param.grad1.sum(dim=0), param.grad))
		exgrads.hooks.deregister(model)

# -------------------------------------
if __name__ == '__main__':
	unittest.main()

# -------------------------------------