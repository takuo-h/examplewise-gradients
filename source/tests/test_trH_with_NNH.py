#! -*- coding:utf-8 -*-

import torch
torch.backends.cudnn.deterministic	= True
torch.backends.cudnn.benchmark		= False

# -------------------------------------------------------------------
import torch
def _diagonal_H(loss, model):
	params = list(model.parameters())
	n = sum(p.numel() for p in params)
	diagH = loss.new_zeros(n)

	k = 0	# index on H, s.t., 0<= k < n
	for i, param in enumerate(params):
		[grad]	= torch.autograd.grad(loss, param, create_graph=True)
		grad	= grad.flatten()

		for j in range(param.numel()):
			if not grad[j].requires_grad: continue

			H_k = torch.autograd.grad(grad[j], params[i], create_graph=False, retain_graph=True)
			H_k = torch.cat([h.flatten() for h in H_k])
			diagH[k] = H_k[j]

			k +=1
	return diagH

def calculate_naively(model,data):
	trHi = []
	for x,y in zip(data.x, data.y):
		x = x.unsqueeze(0)
		y = y.unsqueeze(0)

		logit = model(x)
		loss  = torch.nn.functional.cross_entropy(logit, y, reduction='sum')
		diagH = _diagonal_H(loss, model)

		trHi.append(diagH.sum())
	return trHi

import exgrads
def calculate_by_lib(model,data):
	return exgrads.trH.withNNH(model, data.x)

# -------------------------------------------------------------------
import check_cases
import unittest
class TestMain(unittest.TestCase):
	def setUp(self):		pass
	def tearDown(self):		pass

	def test(self):
		print('='*50)
		error_tolerance = 1e-4

		for name,model,data in check_cases.patterns:
			model.eval()
			naive		= calculate_naively(model, data)
			efficient	= calculate_by_lib(model,  data)
			for i, (n,e) in enumerate(zip(naive, efficient)):
				error = abs(n-e)
				print(f'trH_withNNH   name:{name:>8s} i:{i:02d} error:{error:8.6e}')
				self.assertTrue(error < error_tolerance)
			print('-'*50)

# -------------------------------------
if __name__ == '__main__':
	unittest.main()

# -------------------------------------