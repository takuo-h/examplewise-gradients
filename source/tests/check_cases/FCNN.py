#! -*- coding:utf-8 -*-

import torch
class FCNN(torch.nn.Module):
	def __init__(self, activation=torch.relu):
		super(FCNN, self).__init__()
		self.layer0	= torch.nn.Linear(3,5)
		self.layer1	= torch.nn.Linear(5,5)
		self.layer2	= torch.nn.Linear(5,7)
		self.activation = activation

	def forward(self, x):					# := (b,3)
		x = self.activation(self.layer0(x))	# -> (b,5)
		x = self.activation(self.layer1(x))	# -> (b,5)
		return self.layer2(x)				# -> (b,7)

import torch
from collections import namedtuple
def get_data(data_size=5, input_dim=3):
	x = torch.randn(data_size, input_dim)
	y = torch.randint(low=0,high=7,size=(data_size,))
	return namedtuple('_','x y')(x=x,y=y)

# -------------------------------------------------------------------
cases = []
cases.append( ('FCNN', FCNN(), get_data(),) )
