#! -*- coding:utf-8 -*-

import torch
class CNNwBN(torch.nn.Module):
	def __init__(self, activation=torch.relu, dim=3):
		super(CNNwBN, self).__init__()
		self.layer0	= torch.nn.Conv2d( 1, dim, 3, stride=1, padding=1)
		self.Bnorm0 = torch.nn.BatchNorm2d(dim)
		self.layer1	= torch.nn.Conv2d(dim,dim, 3, stride=1, padding=1)
		self.Bnorm1 = torch.nn.BatchNorm2d(dim)
		self.layer2	= torch.nn.Conv2d(dim,dim, 3, stride=1, padding=1)
		self.Bnorm2 = torch.nn.BatchNorm2d(dim)
		self.layer3	= torch.nn.Linear(dim*5*5, 7)
		self.activation = activation

	def forward(self, x):									# := (b, 1,5,5)
		x = self.activation(self.Bnorm0(self.layer0(x)))	# -> (b, 2,5,5)
		x = self.activation(self.Bnorm1(self.layer1(x)))	# -> (b, 2,5,5)
		x = self.activation(self.Bnorm2(self.layer2(x)))	# -> (b, 2,5,5)
		x = x.flatten(1,-1)									# -> (b, 2*5*5)
		return self.layer3(x)								# -> (b,7)

import torch
from collections import namedtuple
def get_data(data_size=5, image_size=(1,5,5)):
	x = torch.randn(data_size, *image_size)
	y = torch.randint(low=0,high=6,size=(data_size,))
	return namedtuple('_','x y')(x=x,y=y)


# -------------------------------------------------------------------
cases = []
cases.append( ('CNNwBN', CNNwBN(), get_data(),) )
