from . import BN
from . import CNN
from . import FCNN
from . import ResNet
from . import ResNet18

patterns = []
patterns += BN.cases
patterns += CNN.cases
patterns += FCNN.cases
patterns += ResNet.cases
#patterns += ResNet18.cases

__all__ = ['patterns']
